<?php
session_start();
if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logout') {
        unset($_SESSION['userName']);
        unset($_SESSION['fullName']);
        header('Location:index.php');
    }
}
