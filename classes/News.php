<?php
class News {

    protected $link;

    public function __construct() {
        $hostName = 'localhost';
        $userName = "root";
        $password = '';
        $dbName = 'db_dailybangladesh';
        $this->link = mysqli_connect($hostName, $userName, $password, $dbName);
        if (!mysqli_set_charset($this->link, "utf8")) {
            die('Mysqli Charset Problem' . mysqli_error($this->link));
        } else {
            return $this->link;
        }
    }

    private function query($sql) {
        if (!mysqli_query($this->link, $sql)) {
            die("Query problem : " . mysqli_error($this->link));
        }
    }

    private function queryResult($sql) {
        if (mysqli_query($this->link, $sql)) {
            $newsInfo = mysqli_query($this->link, $sql);
            return $newsInfo;
        } else {
            die("Query Problem : " . mysqli_errno($this->link));
        }
    }

    public function addNews($data) {
        $sql = "INSERT INTO tbl_newsoop (newsTitle, authorName, newsDescription, publicationStatus) VALUES ('$data[newsTitle]', '$data[authorName]', '$data[newsDescription]', '$data[publicationStatus]')";
        $this->query($sql);
        $message = "News save successfully";
        return $message;
    }

    public function showNews() {
        $sql = "SELECT * FROM tbl_newsoop ORDER BY newsId DESC";
        $newsInfo = $this->queryResult($sql);
        return $newsInfo;
    }

    public function showNewsById($id) {
        $sql = "SELECT * FROM tbl_newsoop WHERE newsId = '$id'";
        $queryResult = $this->queryResult($sql);
        $newsInfo = mysqli_fetch_assoc($queryResult);
        return $newsInfo;
    }

    public function updateNewsById($data) {
        session_start();
        $authorName = $_SESSION['fullName'];
        $sql = "UPDATE tbl_newsoop SET newsTitle = '$data[newsTitle]', authorName = '$authorName', newsDescription = '$data[newsDescription]', publicationStatus = '$data[publicationStatus]' WHERE newsId = '$data[newsId]'";
        if ($this->queryResult($sql)) {
            session_start();
            $_SESSION['message'] = 'News edited successfully';
            header('Location:manage-news.php');
        }
    }

    public function deleteNewsById($id) {
        $sql = "DELETE FROM tbl_newsoop WHERE newsId = '$id'";
        $deleteNews = $this->queryResult($sql);
        return $deleteNews;
    }

}
