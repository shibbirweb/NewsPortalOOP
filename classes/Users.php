<?php

class Users {

    protected $link;

    public function __construct() {
        $hostName = 'localhost';
        $userName = "root";
        $password = '';
        $dbName = 'db_dailybangladesh';
        $this->link = mysqli_connect($hostName, $userName, $password, $dbName);
        if (!mysqli_set_charset($this->link, "utf8")) {
            die('Mysqli Charset Problem' . mysqli_error($this->link));
        } else {
            return $this->link;
        }
    }

    public function userRegister($data) {
        $password = md5($data['password']);
        $sql = "INSERT INTO tbl_users (fullName, userName, emailAddress, password) VALUES ('$data[fullName]', '$data[userName]', '$data[emailAddress]', '$password')";
        if (!mysqli_query($this->link, $sql)) {
            die("Query Problem : ".mysqli_error($this->link));
        }
        session_start();
        $_SESSION['registrationMessage'] = "Registration complete successfully. Sign in here";
        header('Location:index.php');
    }

    public function userLogin($data) {
        $userName = $data['userName'];
        $password = md5($data['password']);
        $sql = "SELECT * FROM tbl_users WHERE userName = '$userName' AND password = '$password'";
        $result = mysqli_query($this->link, $sql);
        $userInfo = mysqli_fetch_assoc($result);
        if ($userInfo) {
            session_start();
            $_SESSION['fullName'] = $userInfo['fullName'];
            $_SESSION['userName'] = $userInfo['userName'];
            header('Location:add-news.php');
        } else {
            $message = "Invalid username or password";
            return $message;
        }
    }

}
