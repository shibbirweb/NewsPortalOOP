<?php
session_start();

if ($_SESSION['userName'] == NULL OR $_SESSION['fullName'] == NULL) {
    $_SESSION['message'] = "You must be logged in";
    header('Location:index.php');
}

$message = '';

function __autoload($className) {
    require_once 'classes/' . $className . ".php";
}

$objNews = new News();

if (isset($_POST['btn'])) {

    $newsTitle = validation($_POST['newsTitle']);
    $authorName = validation($_POST['authorName']);
    $newsDescription = validation($_POST['newsDescription']);
    $publicationStatus = validation($_POST['publicationStatus']);

    if (!empty($newsTitle) && !empty($authorName) && !empty($newsDescription) && !empty($publicationStatus)) {
        $message = $objNews->addNews($_POST);
    } else {
        $message = "Field must not be empty";
    }
}

function validation($data) {
    $data = htmlspecialchars($data);
    $data = stripcslashes($data);
    $data = trim($data);
    $data = strip_tags($data);
    return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Edit News</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mymenu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Newspaper Portal Site (PHP OOP)</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="mymenu">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li class="active"><a href="add-news.php">Add News<span class="sr-only">(current)</span></a></li>
                        <li><a href="manage-news.php">Manage News</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="">
                                <?php
                                if (isset($_SESSION['fullName'])) {
                                    echo $_SESSION['fullName'];
                                }
                                ?>
                            </a></li>
                        <li><a href="userLogout.php?status=logout">Log out</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="well">
                        <h2 class="text-center text-success">Create News Form</h2>
                        <h3 class="text-center text-success"><?php echo $message; ?></h3>
                        <hr/>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <p class="text-danger">* Required field</p>
                                </div>
                            </div>
                        </div>
                        <form action="" method="POST" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3">News Title <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="newsTitle" class="form-control" placeholder="News title"/>
                                    <input type="hidden" name="id" class="form-control" placeholder="News title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Author Name <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" value="<?php echo $_SESSION['fullName'];?>" name="authorName" class="form-control" placeholder="Author name" readonly="readonly"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">News Description <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="newsDescription" placeholder="News description here"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Publiction Status <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="publicationStatus">
                                        <option>--Select Publication Status--</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>
                                </div>
                            </div>                           
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Create News"/>
                                </div>
                            </div>                           
                        </form>
                        <h6 class="text-center text-muted">&copy; Created by <a href="http://facebook.com/ShibbirAhmedRizwan" target="_blank">Shibbir Ahmed</a> 2018</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/tinymce/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>