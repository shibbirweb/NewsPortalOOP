<?php
session_start();
$message = '';
function __autoload($className) {
    require_once 'classes/' . $className . '.php';
}

$objUser = new Users();

if (isset($_POST['btn'])) {

    $fullName = validation($_POST['fullName']);
    $userName = validation($_POST['userName']);
    $password = validation($_POST['password']);
    $emailAddress = validation($_POST['emailAddress']);

    if (!empty($fullName) && !empty($userName) && !empty($password) && !empty($emailAddress)) {
        $objUser->userRegister($_POST);
    } else {
        $message = "Field must not empty";
    }
}

function validation($data) {
    $data = htmlspecialchars($data);
    $data = stripcslashes($data);
    $data = trim($data);
    $data = strip_tags($data);
    return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="well">
                        <h1 class="text-center text-primary">Newspaper Portal Site (PHP OOP)</h1>
                        <h3 class="text-center text-success">User Registration Form</h3>
                        <h3 class="text-center text-danger">
                            <?php
                            echo $message;
                            if (isset($_SESSION['message']))
                                unset($_SESSION['message']);
                            if (isset($_SESSION['message1']))
                                unset($_SESSION['message1']);
                            ?>
                        </h3>
                        <hr/>
                        <form action="" method="POST" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="fullName" class="form-control" placeholder="Enter your full name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Username</label>
                                <div class="col-sm-9">
                                    <input type="text" name="userName" class="form-control" placeholder="Enter your username"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" class="form-control" placeholder="Enter your password"/>
                                </div>
                            </div>                                                   
                            <div class="form-group">
                                <label class="control-label col-sm-3">Email address</label>
                                <div class="col-sm-9">
                                    <input type="email" name="emailAddress" class="form-control" placeholder="Enter your email address"/>
                                </div>
                            </div>                                                   
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Register"/>
                                </div>
                            </div>
                        </form>
                        <h4 class="text-center text-info">Already have an account ? <a href="index.php">Sign in</a></h4>
                        <h6 class="text-center text-muted">&copy; Created by <a href="http://facebook.com/ShibbirAhmedRizwan" target="_blank">Shibbir Ahmed</a> 2018</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>