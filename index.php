<?php
session_start();

$message = '';

if (isset($_SESSION['userName']) AND isset($_SESSION['fullName'])) {
    if ($_SESSION['userName'] != NULL AND $_SESSION['fullName'] != NULL) {
        header('Location:add-news.php');
    }
} else {
    if (isset($_SESSION['message'])) {
        $message = $_SESSION['message'];
    }
    if (isset($_SESSION['message1'])) {
        $message = $_SESSION['message1'];
    }
    if (isset($_SESSION['registrationMessage'])) {
        $message = $_SESSION['registrationMessage'];
    }
}

function __autoload($className) {
    require_once 'classes/' . $className . '.php';
}

$objUser = new Users();

if (isset($_POST['btn'])) {
    $message = $objUser->userLogin($_POST);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="well">
                        <h1 class="text-center text-primary">Newspaper Portal Site (PHP OOP)</h1>
                        <h3 class="text-center text-success">User Login Form</h3>
                        <h3 class="text-center text-danger">
                            <?php
                            echo $message;

                            if (isset($_SESSION['message']))
                                unset($_SESSION['message']);

                            if (isset($_SESSION['message1']))
                                unset($_SESSION['message1']);

                            if (isset($_SESSION['registrationMessage'])) {
                                unset($_SESSION['registrationMessage']);
                            }
                            ?>
                        </h3>
                        <hr/>
                        <form action="" method="POST" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Username</label>
                                <div class="col-sm-9">
                                    <input type="text" name="userName" class="form-control" placeholder="Username"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                                </div>
                            </div>                                                   
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Sign in"/>
                                </div>
                            </div>
                        </form>
                        <h4 class="text-center text-info">Don't have an account ? <a href="register.php">Sign up</a></h4>
                        <h6 class="text-center text-muted">&copy; Created by <a href="http://facebook.com/ShibbirAhmedRizwan" target="_blank">Shibbir Ahmed</a> 2018</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>