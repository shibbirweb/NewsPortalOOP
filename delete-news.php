<?php

function __autoload($className) {
    require_once 'classes/' . $className . '.php';
}

$objNews = new News();
if (isset($_GET['status'])) {
    if ($_GET['status'] == 'delete' AND $_GET['id']) {
        $id = $_GET['id'];
        $message = "News deleted successfully";
        if ($objNews->deleteNewsById($id)) {
            session_start();
            $_SESSION['message'] = "News delete successfully";
            header("Location:manage-news.php");
        }
    }
}