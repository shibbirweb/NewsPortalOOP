<?php
session_start();

    if ($_SESSION['userName'] == NULL OR $_SESSION['fullName'] == NULL) {
        $_SESSION['message1'] = "You must be logged in";
        header('Location:index.php');
    }

$message = '';

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
}

function __autoload($className) {
    require_once 'classes/' . $className . '.php';
}

$objNews = new News();
$newsInfo = $objNews->showNews();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Manage News</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Newspaper Portal Site (PHP OOP)</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="add-news.php">Add news</a></li>
                        <li class="active"><a href="manage-news.php">Manage news</a></li>
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li><a href=""><?php
                                if (isset($_SESSION['fullName'])) {
                                    echo $_SESSION['fullName'];
                                }
                                ?></a></li>
                        <li><a href="userLogout.php?status=logout">Log out</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="well">
                        <h2 class="text-center text-info">Manage News</h2>
                        <h3 class="text-center text-success"><?php
                            echo $message;
                            if (isset($_SESSION['message']))
                                unset($_SESSION['message']);
                            ?></h3>
                        <hr />
                        <table class="table table-bordered table-striped  table-hover">
                            <thead>
                                <tr>
                                    <th>News Title</th>
                                    <th>Author Name</th>
                                    <th>News Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (mysqli_num_rows($newsInfo) != 0) {
                                    while ($newsInfoFetched = mysqli_fetch_assoc($newsInfo)) {
                                        ?>
                                        <tr align="center">
                                            <td><?php echo $newsInfoFetched['newsTitle']; ?></td>
                                            <td><?php echo $newsInfoFetched['authorName']; ?></td>
                                            <td><?php echo $newsInfoFetched['newsDescription']; ?></td>
                                            <td><?php echo $newsInfoFetched['publicationStatus']; ?></td>
                                            <td>
                                                <a href="edit-news.php?id=<?php echo $newsInfoFetched['newsId']; ?>" class="btn btn-info">
                                                    <span class="glyphicon glyphicon-edit"></span>
                                                </a>
                                                <a href="delete-news.php?status=delete&id=<?php echo $newsInfoFetched['newsId']; ?>" class="btn btn-danger" onclick="return confirm('Are you want to delete this?')">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="5"><h4 class="text-center text-danger">No news available</h4></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <h6 class="text-center text-muted">&copy; Created by <a href="http://facebook.com/ShibbirAhmedRizwan" target="_blank">Shibbir Ahmed</a> 2018</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>